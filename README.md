# Tutorial on 'Ruby on Rails'

This is the second application for the [Ruby on Rails Tutorial](http://www.railstutorial.org/) by [Michael Hartl](http://www.michaelhartl.com/).

## Setting it up on your machine

1. Have Ruby on Rails set up. I suppose you can find a number of guides on how to do that. I've found the one over there helpful: [http://guides.railsgirls.com/install](http://guides.railsgirls.com/install).
2. Get a copy of the repository, on your machine: `git clone https://github.com/phtan/ror-tutorial`
3. Enter the directory-structure: `cd ror-tutorial`
4. Start the app up: `rails server`
0. Browse to the address **http://0.0.0.0:3000/**, in your chosen web-browser.

## Links

- First application under the tutorial: 
[the source-code (on Github)](http://github.com/phtan/ror-tutorial)
- The third application: [the source-code](https://gitlab.com/phtan/sample-app-ror)

## Something about MVC (model-view-controller)

I attempt to complete an exercise in the tutorial (it comes after Listing 2.10 of the tutorial).

Upon visiting the URL */users/1/edit*, with a browser,

1. The action *edit* is executed, in the controller UsersController
2. As there is nothing to be done in that action (as seen from the code, at the time of writing), the controller calls the view, which corresponds to the file *app/views/users/edit.html.erb*
3. The view produces an output of HTML, given the .erb-file and relevant input, such as the variable **<span>&#64;</span>user**.
1. According to Hartl, the HTML passes through the controller to the browser